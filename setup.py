from setuptools import setup


CLASSIFIERS = [
    "Development Status :: 3 - Alpha ",
    "Intended Audience :: Developers",
    "License :: OSI Approved :: BSD License",
    "Operating System :: MacOS :: MacOS X",
    "Operating System :: Microsoft :: Windows",
    "Operating System :: POSIX",
    "Programming Language :: Python :: 2.7",
    "Programming Language :: Python :: 3",
    "Programming Language :: Python :: 3.4",
    "Programming Language :: Python :: 3.5",
    "Programming Language :: Python :: 3.6",
    "Programming Language :: Python :: 3.7",
    "Programming Language :: Python :: 3.8",
    "Programming Language :: Python :: 3.9",
    "Topic :: Database :: Database Engines/Servers",
]


setup(
    name="psycopgbinary",
    description="Reference for psycopg2-binary, but with name usable in import",
    license="BSD License",
    platforms=["posix", "win32", "win64"],
    classifiers=CLASSIFIERS,
    version="0.0.1",
    author="Miloš Korenčiak",
    author_email="milos.korenciak@gmail.com",
    maintainer="Miloš Korenčiak",
    maintainer_email="milos.korenciak@gmail.com",
    keywords=["Postgress", "wrapper", "DB", "psycopg2"],
    install_requires=["psycopg2-binary"],
    py_modules=["psycopgbinary"],
)
